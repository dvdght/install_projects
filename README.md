Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr <br />
https://gitlab.com/hnc/install_projects <br />
http://hnc.toile-libre.org/index.php?section=dev&page=install_projects <br />
https://www.lri.fr/~bagneres/index.php?section=dev&page=install_projects

# Install Projects

This program simplifies the installation of projects on GNU/Linux, Windows, OS X

Apache License, Version 2.0

### System Requirement

Make Life Simpler is written in Python 2.7 <br />
http://www.python.org/

### Installation

No installation needed

### Utilization

See http://hnc.toile-libre.org/index.php?section=dev&page=install_projects <br />
or  https://www.lri.fr/~bagneres/index.php?section=dev&page=install_projects

### Quick Start

Exemple: install project named "hopp"

###### GNU/Linux & OS X

Install Python 2.7, GCC and Git <br />
See http://hnc.toile-libre.org/index.php?section=debian&page=new_install <br />
Or  https://www.lri.fr/~bagneres/index.php?section=debian&page=new_install

Launch a console in scripts directory and run:
```sh
$ cd install_projects/scripts
$ ./install_projects.py install hopp
```

###### Microsoft Windows

Install Python 2.7, GCC (MinGW) and Git <br />
See http://hnc.toile-libre.org/index.php?section=windows&page=new_install <br />
Or  https://www.lri.fr/~bagneres/index.php?section=windows&page=new_install

In file explorer, in "scripts" directory, right-clic and lauch "Git Bash":
```sh
$ /c/Python27/python install_projects.py install hopp
```
