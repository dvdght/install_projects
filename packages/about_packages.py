#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
# Copyright © 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import shutil


# Install Projects packages list
def packages_list():
	return [ 'aonl', 'boost_1.58', 'candl', 'clan', 'clay', 'cloog', 'doxygen', 'hnc', 'hopp', 'ogre_1.9', 'openscop', 'periscop', 'piplib', 'polybench', 'sfml_2.1', 'sfml_2.2', 'sfml_2.3', 'thoth' ]


# About package
def package_about(package, about):
	
	r = ''
	
	# aonl
	if (package == 'aonl'):
		
		if (about == 'description'):
			
			r += 'aonl https://gitlab.com/hnc/aonl' + '\n'
			r += '- Automatically Optimized Numerical Library'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# autoconf
	elif (package == 'autoconf'):
		
		if (about == 'description'):
			
			r += 'Autoconf http://www.gnu.org/software/autoconf/' + '\n'
			r += '- Produce scripts to automatically configure software source code packages'
			
		elif (about == 'name_on_debian'):
			
			r += 'autoconf'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'autoconf'
	
	# automake
	elif (package == 'automake'):
		
		if (about == 'description'):
			
			r += 'Automake http://www.gnu.org/software/automake/' + '\n'
			r += '- Automatically generating Makefile.in files (uses Autoconf)'
			
		elif (about == 'name_on_debian'):
			
			r += 'automake'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'automake'
	
	# bison
	elif (package == 'bison'):
		
		if (about == 'description'):
			
			r += 'GNU Bison http://en.wikipedia.org/wiki/GNU_bison' + '\n'
			r += '- Reads a specification of a context-free language, warns about any' + '\n'
			r += '  parsing ambiguities, and generates a parser (either in C, C++, or Java)'
			
		elif (about == 'name_on_debian'):
			
			r += 'bison'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'bison'
	
	# boost
	elif ('boost' in package):
		
		if (about == 'description'):
			
			r += 'Boost http://www.boost.org/' + '\n'
			r += '- "...one of the most highly regarded and expertly designed' + '\n'
			r += '  C++ library projects in the world"' + '\n'
			r += '  Herb Sutter and Andrei Alexandrescu, C++ Coding Standards'
			
		elif (about == 'name_on_debian'):
			
			r += 'libboost-all-dev libboost-mpi-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'boost'
	
	# candl
	elif (package == 'candl'):
		
		if (about == 'description'):
			
			r += 'Candl http://icps.u-strasbg.fr/people/bastoul/public_html/development/candl/' + '\n'
			r += '- Data Dependence Analyzer in the Polyhedral Model'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# clan
	elif (package == 'clan'):
		
		if (about == 'description'):
			
			r += 'Clan http://icps.u-strasbg.fr/people/bastoul/public_html/development/clan/' + '\n'
			r += '- Chunky Loop Analyzer, a Polyhedral Representation Extraction Tool for High Level Programs'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# clan
	elif (package == 'clay'):
		
		if (about == 'description'):
			
			r += 'Clay http://icps.u-strasbg.fr/~bastoul/development/clay/index.html' + '\n'
			r += '- Clay, the Chunky Loop Alteration wizardrY'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# cloog
	elif (package == 'cloog'):
		
		if (about == 'description'):
			
			r += 'CLooG http://www.cloog.org/' + '\n'
			r += '- The CLooG Code Generator in the Polytope Model'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# doxygen
	elif (package == 'doxygen'):
		
		if (about == 'description'):
			
			r += 'Doxygen http://www.stack.nl/~dimitri/doxygen/index.html' + '\n'
			r += '- Generate documentation from source code'
			
		elif (about == 'name_on_debian'):
			
			r += 'doxygen'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'doxygen'
	
	# cmake
	elif (package == 'cmake'):
		
		if (about == 'description'):
			
			r += 'CMake http://www.cmake.org/' + '\n'
			r += '- A cross-platform, open-source build system'
			
		elif (about == 'name_on_debian'):
			
			r += 'cmake'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'cmake'
	
	# cppunit
	elif (package == 'cppunit'):
		
		if (about == 'description'):
			
			r += 'CppUnit http://sourceforge.net/apps/mediawiki/cppunit/index.php' + '\n'
			r += '- C++ unit testing framework'
			
		elif (about == 'name_on_debian'):
			
			r += 'libcppunit-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'cppunit'
	
	# flex
	elif (package == 'flex'):
		
		if (about == 'description'):
			
			r += 'Flex http://flex.sourceforge.net/' + '\n'
			r += '- The Fast Lexical Analyzer'
			
		elif (about == 'name_on_debian'):
			
			r += 'flex'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'flex'
	
	# freeimage
	elif (package == 'freeimage'):
		
		if (about == 'description'):
			
			r += 'FreeImage http://freeimage.sourceforge.net/' + '\n'
			r += '- Open Source library to support popular graphics image formats' + '\n'
			r += 'like PNG, BMP, JPEG, TIFF and others'
			
		elif (about == 'name_on_debian'):
			
			r += 'libfreeimage-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'freeimage'
	
	# freetype
	elif (package == 'freetype'):
		
		if (about == 'description'):
			
			r += 'FreeType http://www.freetype.org/' + '\n'
			r += '- Capable of producing high-quality output (glyph images)'
			
		elif (about == 'name_on_debian'):
			
			r += 'libfreetype6-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'freetype'
	
	# gcc
	elif (package == 'gcc'):
		
		if (about == 'description'):
			
			r += 'GCC https://gcc.gnu.org/' + '\n'
			r += '- The GNU Compiler Collection includes front ends for C, C++,' + '\n'
			r += '  Objective-C, Fortran, Java, Ada, and Go,' + '\n'
			r += '  as well as libraries for these languages (libstdc++, libgcj, ...)'
			
		elif (about == 'name_on_debian'):
			
			r += 'build-essential'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'gcc49'
	
	# git
	elif (package == 'git'):
		
		if (about == 'description'):
			
			r += 'Git http://git-scm.com/' + '\n'
			r += '- Free and open source distributed version control system'
			
		elif (about == 'name_on_debian'):
			
			r += 'git'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'git'
	
	# glew
	elif (package == 'glew'):
		
		if (about == 'description'):
			
			r += 'GLEW http://glew.sourceforge.net/' + '\n'
			r += '- A portable command-line driven graphing utility'
			
		elif (about == 'name_on_debian'):
			
			r += 'libglew-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'glew'
	
	# glsl-optimizer
	elif (package == 'glsl-optimizer'):
		
		if (about == 'description'):
			
			r += 'GLSL optimizer https://github.com/aras-p/glsl-optimizer' + '\n'
			r += '- A C++ library that takes GLSL shaders, does some GPU-independent optimizations on them and outputs GLSL back'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# gnuplot
	elif (package == 'gnuplot'):
		
		if (about == 'description'):
			
			r += 'Gnuplot http://www.gnuplot.info/' + '\n'
			r += 'The OpenGL Extension Wrangler Library'
			
		elif (about == 'name_on_debian'):
			
			r += 'gnuplot'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'gnuplot'
	
	# gmp
	elif (package == 'gmp'):
		
		if (about == 'description'):
			
			r += 'GMP https://gmplib.org/' + '\n'
			r += '- Free library for arbitrary precision arithmetic, operating on' + '\n'
			r += '  signed integers, rational numbers, and floating-point numbers'
			
		elif (about == 'name_on_debian'):
			
			r += 'libgmp-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'gmp'
	
	# graphviz
	elif (package == 'graphviz'):
		
		if (about == 'description'):
			
			r += 'Graphviz http://www.graphviz.org/' + '\n'
			r += '- An open source graph visualization software'
			
		elif (about == 'name_on_debian'):
			
			r += 'graphviz'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'graphviz'
	
	# hlsl2glsl
	elif (package == 'hlsl2glsl'):
		
		if (about == 'description'):
			
			r += 'HLSL2GLSL https://github.com/aras-p/hlsl2glslfork' + '\n'
			r += '- HLSL to GLSL shader language translator'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# hnc
	elif (package == 'hnc'):
		
		if (about == 'description'):
			
			r += 'hnc http://hnc.toile-libre.org/index.php?section=dev&page=hnc' + '\n'
			r += '- Basic (but useful) C++11 header-only library'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# hopp
	elif (package == 'hopp'):
		
		if (about == 'description'):
			
			r += 'hopp https://gitlab.com/hnc/hopp' + '\n'
			r += '- Header-Only Program Parts' + '\n'
			r += '  Basic (but useful), modern and generic C++11/C++14 header-only library'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# libjpeg
	elif (package == 'libjpeg'):
		
		if (about == 'description'):
			
			r += 'libjpeg http://libjpeg.sourceforge.net/' + '\n'
			r += '- C library for reading and writing JPEG image files'
			
		elif (about == 'name_on_debian'):
			
			r += 'libjpeg-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# libsndfile
	elif (package == 'libsndfile'):
		
		if (about == 'description'):
			
			r += 'libsndfile http://www.mega-nerd.com/libsndfile/' + '\n'
			r += '- C library for reading and writing files containing sampled sound'
			
		elif (about == 'name_on_debian'):
			
			r += 'libsndfile-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'libsndfile'
	
	# libtool
	elif (package == 'libtool'):
		
		if (about == 'description'):
			
			r += 'GNU Libtool http://www.gnu.org/software/libtool/' + '\n'
			r += '- Hides the complexity of using shared libraries behind a consistent, portable interface'
			
		elif (about == 'name_on_debian'):
			
			r += 'libtool'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'libtool'
	
	# mercurial
	elif (package == 'mercurial'):
		
		if (about == 'description'):
			
			r += 'Mercurial http://mercurial.selenic.com/' + '\n'
			r += '- A free, distributed source control management tool'
			
		elif (about == 'name_on_debian'):
			
			r += 'mercurial'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'mercurial'
	# mingw
	elif (package == 'mingw'):
		
		if (about == 'description'):
			
			r += 'MinGW http://www.mingw.org/ http://sourceforge.net/projects/mingwbuilds/' + '\n'
			r += '- Minimalist GNU for Windows' + '\n'
			r += '  Includes a port of the GNU Compiler Collection (GCC)'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# ogre
	elif ('ogre' in package):
		
		if (about == 'description'):
			
			r += 'OGRE http://www.ogre3d.org/' + '\n'
			r += '- One of the most popular open-source graphics rendering engines'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# ois
	elif (package == 'ois'):
		
		if (about == 'description'):
			
			r += 'OIS http://sourceforge.net/projects/wgois/' + '\n'
			r += '- Object Oriented Input System'
			
		elif (about == 'name_on_debian'):
			
			r += 'libois-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'ois'
	
	# openal
	elif (package == 'openal'):
		
		if (about == 'description'):
			
			r += 'OpenAL http://kcat.strangesoft.net/openal.html' + '\n'
			r += '- Software implementation of the OpenAL 3D audio API'
			
		elif (about == 'name_on_debian'):
			
			r += 'libopenal-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'openal'
	
	# opengl
	elif (package == 'opengl'):
		
		if (about == 'description'):
			
			r += 'OpenGL http://www.opengl.org/' + '\n'
			r += '- A cross-language, multi-platform application programming interface (API)'
			r += '  for rendering 2D and 3D vector graphics'
			
		elif (about == 'name_on_debian'):
			
			r += 'libgl-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# openscop
	elif (package == 'openscop'):
		
		if (about == 'description'):
			
			r += 'OpenScop http://icps.u-strasbg.fr/~bastoul/development/openscop/index.html' + '\n'
			r += '- A Specification and a Library for Data Exchange in Polyhedral Compilation Tools'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# openssl
	elif (package == 'openssl'):
		
		if (about == 'description'):
			
			r += 'OpenSSL http://www.openssl.org/' + '\n'
			r += '- SSL, TLS, cryptography library'
			
		elif (about == 'name_on_debian'):
			
			r += 'libssl-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'openssl'
	
	# perl
	elif (package == 'perl'):
		
		if (about == 'description'):
			
			r += 'Perl https://www.perl.org/' + '\n'
			r += '- The Perl Programming Language'
			
		elif (about == 'name_on_debian'):
			
			r += 'perl'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'perl5'
	
	# piplib
	elif (package == 'piplib'):
		
		if (about == 'description'):
			
			r += 'PipLib http://www.piplib.org/' + '\n'
			r += '- Parametric Integer Programming Library'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# pkg-config
	elif (package == 'pkg-config'):
		
		if (about == 'description'):
			
			r += 'pkg-config http://www.freedesktop.org/wiki/Software/pkg-config/' + '\n'
			r += '- A helper tool used when compiling applications and libraries'
			
		elif (about == 'name_on_debian'):
			
			r += 'pkg-config'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'rb-pkg-config'
	
	# poco
	elif (package == 'poco'):
		
		if (about == 'description'):
			
			r += 'POCO http://pocoproject.org/' + '\n'
			r += '- Modern, powerful open source C++ class libraries and frameworks for' + '\n'
			r += '  building network- and internet-based applications that run on desktop,' + '\n'
			r += '  server, mobile and embedded systems'
			
		elif (about == 'name_on_debian'):
			
			r += 'libpoco-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'poco'
	
	# python
	elif (package == 'python'):
		
		if (about == 'description'):
			
			r += 'Python https://www.python.org/' + '\n'
			r += '- A programming language that lets you work quickly'
			r += '  and integrate systems more effectively'
			
		elif (about == 'name_on_debian'):
			
			r += 'python python2.7'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'python27'
	
	# sfml
	elif ('sfml' in package):
		
		if (about == 'description'):
			
			r += 'SFML http://www.sfml-dev.org/index.php' + '\n'
			r += '- Simple and Fast Multimedia Library'
			
		elif (about == 'name_on_debian'):
			
			r += 'libsfml-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# tbb
	elif (package == 'tbb'):
		
		if (about == 'description'):
			
			r += 'TBB https://www.threadingbuildingblocks.org/' + '\n'
			r += '- To write easily parallel C++ programs that take full advantage of' + '\n'
			r += '  multicore performance, that are portable and composable,' + '\n'
			r += '  and that have future-proof scalability'
			
		elif (about == 'name_on_debian'):
			
			r += 'libtbb-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'tbb'
	
	# thoth
	elif (package == 'thoth'):
		
		if (about == 'description'):
			
			r += 'Thōth https://gitlab.com/hnc/thoth' + '\n'
			r += '- Modern and generic C++11/C++14 library for GUI and games based on SFML'
			
		elif (about == 'name_on_debian'):
			
			r += ''
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# tinyxml
	elif (package == 'tinyxml'):
		
		if (about == 'description'):
			
			r += 'TinyXML http://www.grinninglizard.com/tinyxml/' + '\n'
			r += '- Simple, small, C++ XML parser that can be easily integrating'
			
		elif (about == 'name_on_debian'):
			
			r += 'libtinyxml-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'tinyxml'
	
	# udev
	elif (package == 'udev'):
		
		if (about == 'description'):
			
			r += 'libudev http://www.freedesktop.org/software/systemd/libudev/' + '\n'
			r += '- A device manager for the Linux kernel'
			
		elif (about == 'name_on_debian'):
			
			r += 'libudev-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += ''
	
	# x11
	elif (package == 'x11'):
		
		if (about == 'description'):
			
			r += 'X11 http://www.x.org/wiki/' + '\n'
			r += '- An open source implementation of the X Window System' + '\n'
			r += 'libXext http://cgit.freedesktop.org/xorg/lib/libXext/' + '\n'
			r += '- Common X Extensions library' + '\n'
			r += 'XRandR http://www.x.org/wiki/Projects/XRandR/' + '\n'
			r += '- An official configuration utility to the RandR X Window System extension' + '\n'
			r += 'libXt http://cgit.freedesktop.org/xorg/lib/libXt/' + '\n'
			r += '- X Toolkit Intrinsics library' + '\n'
			r += 'libXaw http://cgit.freedesktop.org/xorg/lib/libXaw/' + '\n'
			r += '- Athena Widgets toolkit'
			
		elif (about == 'name_on_debian'):
			
			r += 'libx11-dev libxext-dev libxrandr-dev libxt-dev libxaw7-dev libx11-xcb-dev libxcb-image0-dev xcb'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'xorg xorg-libX11 xorg-libXext xorg-libXrandr xorg-libXt xorg-libXaw'
	
	# zlib
	elif (package == 'zlib'):
		
		if (about == 'description'):
			
			r += 'zlib http://www.zlib.net/' + '\n'
			r += '- A Massively Spiffy Yet Delicately Unobtrusive Compression Library'
			
		elif (about == 'name_on_debian'):
			
			r += 'zlib1g-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'zlib'
	
	# zziplib
	elif (package == 'zziplib'):
		
		if (about == 'description'):
			
			r += 'zziplib http://zziplib.sourceforge.net/' + '\n'
			r += '- Lightweight library to easily extract data from files archived in a single zip file'
			
		elif (about == 'name_on_debian'):
			
			r += 'libzzip-dev'
			
		elif (about == 'name_on_os_x_with_macports'):
			
			r += 'libzzip'
		
	else:
		
		print 'Install Projects: error: package_about: unknown package "' + package + '"'
	
	return r


# Package description
def package_description(package):
	
	return package_about(package, 'description')


# Package name on Debian
def package_name_on_debian(package):
	
	return package_about(package, 'name_on_debian')


# Package name on OS X with MacPorts
def package_name_on_os_x_with_macports(package):
	
	return package_about(package, 'name_on_os_x_with_macports')


# Download package
def download_package(package):
	
	from make_life_simpler import filesystem
	from make_life_simpler.download import download
	
	current_dir = os.path.abspath(os.getcwd())
	packages_directory = os.path.join(filesystem.app_data('install_projects'), 'packages')
	if (not os.path.exists(packages_directory)): os.makedirs(packages_directory)
	os.chdir(packages_directory)
	
	open('__init__.py', 'w').close()
	
	install_projects_www = 'https://gitlab.com/hnc/install_projects/raw/master/packages/'
	
	try:
		
		download(install_projects_www, package + '.py')
		
	except:
		
		print 'Install_project: download_package: warning: Package "' + package + '" not found :('
		
	os.chdir(current_dir)

# Copy package
def copy_package(package_directory, package):
	
	from make_life_simpler import filesystem
	
	current_dir = os.path.abspath(os.getcwd())
	packages_directory = os.path.join(filesystem.app_data('install_projects'), 'packages')
	if (not os.path.exists(packages_directory)): os.makedirs(packages_directory)
	os.chdir(packages_directory)
	
	open('__init__.py', 'w').close()
	
	try:
		
		shutil.copyfile(os.path.join(package_directory, package + '.py'), package + '.py')
		
	except:
		
		print 'Install_project: copy_package: warning: Package "' + package + '" not found :('
		
	os.chdir(current_dir)

# Download packages
def download_packages(script_dir):
	
	# Download
	for package in packages_list():
		download_package(package)
	
	# Local
	local_packages_dir = os.path.join(script_dir, os.pardir, 'packages')
	if (os.path.isdir(local_packages_dir)):
		for package in packages_list():
			copy_package(local_packages_dir, package)
