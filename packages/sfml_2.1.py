#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Dependencies
global dependencies
dependencies = []
#dependencies += ['dependency_name']
dependencies += ['freetype']
dependencies += ['glew']
dependencies += ['libjpeg']
dependencies += ['libsndfile']
dependencies += ['openal']
import sys
if (sys.platform == 'linux2'):
	dependencies += ['opengl']
	dependencies += ['x11']

# Optional dependencies
global dependencies_optional
dependencies_optional = []
#dependencies_optional += ['dependency_name']

# Download archive
global archive_dirname
archive_dirname = 'SFML-2.1'
global archive_url_dirname
archive_url_dirname = 'http://www.sfml-dev.org/files/'
global archive_url_filename
archive_url_filename = 'SFML-2.1-sources.zip'

# Build
global how_build
how_build = 'CUSTOM_BUILD'
import os
def how_build_function(install_directory):
	from make_life_simpler.build import build_with_cmake
	from make_life_simpler.system import system
	if (os.name == 'posix'):
		if (sys.platform == 'darwin'):
			if (os.path.exists('/opt/local/include/freetype') and not os.path.exists(install_directory + '/include/freetype2')):
				system(['ln', '-s', '/opt/local/include/freetype2', install_directory + '/include/freetype'])
			if (os.path.exists('/opt/local/include/freetype') and not os.path.exists(install_directory + '/include/freetype2')):
				system(['sudo', 'ln', '-s', '/opt/local/include/freetype2', install_directory + '/include/freetype'])
			build_with_cmake('SFML-2.1', install_directory)
		else:
			build_with_cmake('SFML-2.1', install_directory, '', '', ['-DFREETYPE_INCLUDE_DIRS=/usr/include/freetype2'])
	else:
		build_with_cmake('SFML-2.1', install_directory)
