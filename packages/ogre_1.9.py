#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2013, 2014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr
# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Dependencies
global dependencies
dependencies = []
#dependencies += ['dependency_name']
dependencies += ['ois']
dependencies += ['zlib']
dependencies += ['zziplib']
dependencies += ['freeimage']
dependencies += ['freetype']
dependencies += ['opengl']
dependencies += ['tinyxml']
dependencies += ['boost']
dependencies += ['poco']
dependencies += ['tbb']
dependencies += ['cppunit']
import sys
if (sys.platform == 'linux2'):
	dependencies += ['opengl']
	dependencies += ['x11']

# Optional dependencies
global dependencies_optional
dependencies_optional = []
#dependencies_optional += ['dependency_name']
dependencies_optional += ['glsl-optimizer']
dependencies_optional += ['hlsl2glsl']

# Mercurial repository
global hg_repository_name
hg_repository_name = 'ogre'
global git_repository
hg_repository = 'https://bitbucket.org/sinbad/ogre/'
def hg_repository_extra_function():
	from make_life_simpler.system import system_halt_on_error
	system_halt_on_error(['hg', 'update', 'v1-9'])

# Build
global how_build
how_build = 'BUILD_WITH_CMAKE'
