#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Dependencies
global dependencies
dependencies = []
#dependencies += ['dependency_name']

# Optional dependencies
global dependencies_optional
dependencies_optional = []
#dependencies_optional += ['dependency_name']

# Download archive
global archive_dirname
archive_dirname = 'boost_1_58_0'
global archive_url_dirname
archive_url_dirname = 'https://www.lri.fr/~bagneres/files/dev/install_projects/packages/'
global archive_url_filename
archive_url_filename = 'boost_1_58_0.zip'

# Build
global how_build
how_build = 'CUSTOM_BUILD'
import os

def how_build_function(install_directory):
	
	from make_life_simpler.system import system
	
	os.chdir(archive_dirname)
	
	if (os.name == 'posix'):
		
		system(['./bootstrap.sh', '--prefix=' + install_directory])
		system(['./b2', 'install'])
	
	elif (os.name == 'nt'):
		
		system(['bootstrap.bat', 'mingw'])
		system(['b2', 'install', '--prefix=' + install_directory, 'toolset=gcc'])
		
	else:
		print 'Your platform "' + os.name + '" is not supported'
		
	os.chdir(os.pardir)
