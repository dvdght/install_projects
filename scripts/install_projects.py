#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright © 2014 Lénaïc Bagnères, hnc@singularity.fr
# Copyright © 014 Inria, Written by Lénaïc Bagnères, lenaic.bagneres@inria.fr

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Minimal version of download function from https://gitlab.com/hnc/make_life_simpler
import os
import urllib
import distutils.spawn
def download_(url, filename):
	try:
		image = urllib.URLopener()
		image.retrieve(url + filename, filename)
	except:
		if (distutils.spawn.find_executable('wget') != None):
			if (os.system('wget ' + url + filename + ' -O ' + filename) != 0): os.remove(filename)
		elif (distutils.spawn.find_executable('curl') != None):
			os.system('curl -L "' + url + filename + '" -o "' + filename + '"')
		elif (os.name == 'nt'):
			os.system('powershell -Command (new-object System.Net.WebClient).DownloadFile(\'' + url + filename + '\',\'' + filename + '\')')
		else: raise
	if (not os.path.exists(filename)): raise

# Minimal version of filesystem.home function from https://gitlab.com/hnc/make_life_simpler
import os
def filesystem_home_(): return os.path.expanduser('~')

# Minimal version of filesystem.app_data function from https://gitlab.com/hnc/make_life_simpler
import os
def filesystem_app_data_(application_name):
	if (os.name == 'posix'): return filesystem_home_() + '/.' + application_name
	elif (os.name == 'nt'): return os.path.join(os.environ['APPDATA'], application_name)
	else:
		print 'filesystem_app_data_: error: the operating system "' + os.name + '" is not supported'
		raise


import os


# Check and downloads dependencies
def download_dependencies(script_dir):
	
	# make_life_simpler
	
	if (not os.path.exists('make_life_simpler')): os.makedirs('make_life_simpler')
	
	mls_www = 'https://gitlab.com/hnc/make_life_simpler/raw/master/bin/'
	
	download_(mls_www, 'can_write_in.py')
	download_(mls_www, 'cmd_with_root_access.py')
	download_(mls_www, 'exe_is_available.py')
	download_(mls_www, 'extract.py')
	download_(mls_www, 'get_exe.py')
	
	download_(mls_www, 'make_life_simpler/__init__.py')
	download_(mls_www, 'make_life_simpler/abort_if_no.py')
	download_(mls_www, 'make_life_simpler/args.py')
	download_(mls_www, 'make_life_simpler/ask_user.py')
	download_(mls_www, 'make_life_simpler/build.py')
	download_(mls_www, 'make_life_simpler/download.py')
	download_(mls_www, 'make_life_simpler/env.py')
	download_(mls_www, 'make_life_simpler/filesystem.py')
	download_(mls_www, 'make_life_simpler/system.py')
	
	# install_projects
	
	download_('https://gitlab.com/hnc/install_projects/raw/master/packages/', 'about_packages.py')
	
	# install_projects packages
	
	sys.path.append('.')
	from about_packages import download_packages
	download_packages(script_dir)


# Choose install directory
def choose_install_directory():
	
	from make_life_simpler.ask_user import yes_no_question
	from make_life_simpler.ask_user import choice_question
	from make_life_simpler.abort_if_no import abort_if_no
	from make_life_simpler import filesystem
	from make_life_simpler.system import system
	from make_life_simpler.system import system_halt_on_error
	sys.path.insert(1, os.path.join(sys.path[0], '..'))
	from cmd_with_root_access import cmd_with_root_access_if_needed
	
	choices = []
	
	# POSIX
	if (os.name == 'posix'):
		
		choices += [os.path.expanduser('~') + '/usr']
		
		if (sys.platform == 'darwin'): choices += ['/opt/local']
		
		choices += ['/usr/local']
		
	# NT
	elif (os.name == 'nt'):
		
		choices += [filesystem.home() + '\\usr']
		choices += [filesystem.home()[0] + ':\\Users\\Public\\usr']
		
	# Other
	else:
		
		print 'Error: install_projects.choose_install_directory: the operating system "' + os.name + '" is not supported'
		sys.exit(0)
	
	choices += ['Custom path']
	
	choice = choice_question(choices, 'Choose the install directory:')
	
	print ''
	
	if (choice < len(choices) - 1):
		
		directory = choices[choice]
	
	else:
		
		directory = raw_input('New install directory = ')
		abort_if_no(directory != '', 0, 'Install directory is empty, aborting...')
		directory = os.path.abspath(os.path.expanduser(directory))
		print ''
	
	print 'Install directory =', directory
	
	if (not os.path.exists(directory)):
		if (os.name == 'posix'):
			system(['mkdir', '-p', directory])
			system_halt_on_error(cmd_with_root_access_if_needed(['mkdir', '-p', directory], directory))
		else:
			os.makedirs(directory)
	
	return directory

# Choose C compiler
def choose_c_compiler():
	
	from make_life_simpler.ask_user import yes_no_question
	
	c_compiler = ''
	
	custom_c_compiler = yes_no_question('Do you want use a custom C compiler?')
	
	if (custom_c_compiler):
		
		c_compiler = raw_input('Custom C compiler = ')
		
		print 'Your C compiler is:'
		system_halt_on_error([c_compiler, '-v'])
	
	return c_compiler

# Choose C++ compiler
def choose_cpp_compiler():
	
	from make_life_simpler.ask_user import yes_no_question
	
	cpp_compiler = ''
	
	custom_c_compiler = yes_no_question('Do you want use a custom C++ compiler?')
	
	if (custom_c_compiler):
		
		cpp_compiler = raw_input('Custom C++ compiler = ')
		
		print 'Your C++ compiler is:'
		system_halt_on_error([cpp_compiler, '-v'])
	
	return cpp_compiler

# Dependencies
def ask_for_dependencies(dependencies):
	
	from about_packages import package_description
	from about_packages import package_name_on_debian
	from about_packages import package_name_on_os_x_with_macports
	
	if (dependencies != []):
		
		print ''
		
		for dependency in dependencies:
			
			print package_description(dependency)
			print ''
			
		if (sys.platform == 'linux2'):
			
			packages_debian = ''
			packages_install_projects = ''
			
			for dependency in dependencies:
				tmp = package_name_on_debian(dependency)
				if (tmp != ''): packages_debian += tmp + ' '
				else: packages_install_projects += dependency + ' '
			
			if (packages_debian != ''):
				print 'On Debian GNU/Linux, please install these packages (aptitude install):'
				print packages_debian
				print ''
			
			if (packages_install_projects != ''):
				print 'Please install these packages with Install Projects:'
				print packages_install_projects
				print ''
			
		elif (sys.platform == 'darwin'):
			
			packages_macports = ''
			packages_install_projects = ''
			
			for dependency in dependencies:
				tmp = package_name_on_os_x_with_macports(dependency)
				if (tmp != ''): packages_macports += tmp + ' '
				else: packages_install_projects += dependency + ' '
			
			if (packages_macports != ''):
				print 'On OS X, please install these packages with MacPorts:'
				print packages_macports
				print ''
			
			if (packages_install_projects != ''):
				print 'Please install these packages with Install Projects:'
				print packages_install_projects
				print ''
		
		abort_if_no(yes_no_question('These packages are installed?'))

# Display help
def display_help():
	
	print ''
	print 'Usage:', sys.argv[0], '<install | clean | clear | reset>',  '[command arguments]'
	print ''
	print '  install <package_name_a package_name_b ...> # Install package a and b'
	print ''
	print '  search  <package_name>                      # Search package'
	print '  list                                        # List all packages'
	print ''
	print '  clean                                       # Remove all build directories'
	print '  clear                                       # Remove all package directories'
	print '  reset                                       # Remove all files of Install Projects'
	print ''


import os
import sys
import copy


# Program
if __name__ == "__main__":
	
	# Working directory for download, extract, compile all projects
	script_dir = os.path.dirname(os.path.realpath(__file__))
	current_dir = os.path.abspath(os.getcwd())
	work_dir = filesystem_app_data_('install_projects')
	if (not os.path.exists(work_dir)): os.makedirs(work_dir)
	os.chdir(work_dir)
	
	# Download dependencies
	download_dependencies(script_dir)
	# Import
	sys.path.append('.')
	from make_life_simpler import args
	from make_life_simpler.ask_user import yes_no_question
	from make_life_simpler.build import *
	from make_life_simpler.download import *
	from make_life_simpler.env import *
	from extract import *
	from make_life_simpler import filesystem
	from make_life_simpler.system import *
	from about_packages import *
	
	
	# Help
	if args.no_args() or args.help() or (len(sys.argv) > 1 and sys.argv[1] == 'help'):
		
		display_help()
		sys.exit(0)
	
	print ''
	
	
	# Install directory
	install_directory = ''
	if (not os.path.exists('install_directory')):
		print 'This is the first run of Install Projects, choose the install directory'
		install_directory = choose_install_directory()
		file = open('install_directory', 'w')
		file.write(install_directory + '\n')
		file.close()
	else:
		file = open('install_directory', 'r')
		install_directory = file.readline()
		file.close()
		install_directory = install_directory.replace('\n', '')
		print 'Install directory already configured =', install_directory
	print ''
	
	# Priority over the system
	priority_over_the_system = False
	if (not os.path.exists('priority_over_the_system')):
		priority_over_the_system = yes_no_question('The user installation has the priority over the system?')
		file = open('priority_over_the_system', 'w')
		file.write(str(priority_over_the_system) + '\n')
		file.close()
	else:
		file = open('priority_over_the_system', 'r')
		priority_over_the_system = file.readline()
		file.close()
		priority_over_the_system = priority_over_the_system.replace('\n', '')
		if (priority_over_the_system == 'True'): priority_over_the_system = True
		else: priority_over_the_system = False
		print 'The user installation has the priority over the system =', priority_over_the_system
	update_variables(install_directory, priority_over_the_system)
	
	# Modify environment variables automatically at startup
	if (not os.path.exists('env_auto')):
		env_auto = yes_no_question('Do you want modify environment variables automatically at startup?')
		if (env_auto):
			update_variables_in_config_files(install_directory, priority_over_the_system)
			update_variables_in_config_files_help()
		else:
			print 'You can execute these commands to update yours environments variables:'
			update_variables_display(install_directory, priority_over_the_system)
		open('env_auto', 'w').close()
	
	print ''
	
	# clean and clear
	print 'If something goes wront, use clean and/or clear and/or reset command'
	print 'If the problem persists'
	
	# Arguments
	command = sys.argv[1]
	
	
	# install
	if (command == 'install'):
		
		# Get packages
		packages = sys.argv[2:]
		
		i = 0
		while i < len(packages):
			
			package = packages[i]
			
			# Download package
			#print 'Download package "' + package + '"'
			#download_package(package)
			#print '- done =)'
			
			# Unload package
			try: del tmp.other_packages
			except: pass
			try: del tmp.dependencies
			except: pass
			try: del tmp.dependencies_optional
			except: pass
			try: del tmp.archive_dirname
			except: pass
			try: del tmp.archive_url_dirname
			except: pass
			try: del tmp.archive_url_filename
			except: pass
			try: del tmp.git_repository_name
			except: pass
			try: del tmp.git_repository
			except: pass
			try: del tmp.hg_repository_name
			except: pass
			try: del tmp.hg_repository
			except: pass
			try: del tmp.hg_repository_extra_function
			except: pass
			try: del tmp.how_build
			except: pass
			try: del tmp.how_build_autotools_configure_args_list
			except: pass
			try: del tmp.how_build_function
			except: pass
			
			# Load package
			import shutil
			if (os.path.exists(os.path.join('packages', 'tmp.py'))): os.remove(os.path.join('packages', 'tmp.py'))
			if (os.path.exists(os.path.join('packages', 'tmp.pyc'))): os.remove(os.path.join('packages', 'tmp.pyc'))
			#os.system('sync')
			shutil.copyfile(os.path.join('packages', package + '.py'), os.path.join('packages', 'tmp.py'))
			import packages.tmp as tmp
			reload(tmp)
			print ''
			
			# Install
			print 'Install package "' + package + '"'
			
			# Meta-package
			is_meta_package = False
			try:
				tmp.other_packages
				is_meta_package = True
				print 'Replace meta-package \"' + package + '\" by ' + str(tmp.other_packages)
				packages = copy.deepcopy(packages[0:i] + tmp.other_packages + packages[i+1:])
			except: pass
			if (is_meta_package == True):
				continue
			print ''
			
			# Dependencies
			print 'Dependencies:', tmp.dependencies
			ask_for_dependencies(tmp.dependencies)
			
			# Optional dependencies
			print ''
			print 'Optional dependencies:', tmp.dependencies_optional
			ask_for_dependencies(tmp.dependencies_optional)
			print ''
			
			# Download
			print 'Download:'
			build_directory = ''
			try:
				build_directory = tmp.archive_dirname
				if (not os.path.exists(tmp.archive_dirname)):
					download(tmp.archive_url_dirname, tmp.archive_url_filename)
					extract(tmp.archive_url_filename)
			except:
				print '- No archive to be downloaded'
			# Git
			try:
				build_directory = tmp.git_repository_name
				print '- Git repository:', tmp.git_repository
				if (not os.path.exists(tmp.git_repository_name)):
					system_halt_on_error(['git', 'clone', tmp.git_repository])
				else:
					os.chdir(tmp.git_repository_name)
					system_halt_on_error(['git', 'pull'])
					os.chdir(work_dir)
			except:
				print '- No git repository'
			# Mercurial
			try:
				build_directory = tmp.hg_repository_name
				print '- Mercurial repository:', tmp.hg_repository
				if (not os.path.exists(tmp.hg_repository_name)):
					system_halt_on_error(['hg', 'clone', tmp.hg_repository])
				else:
					os.chdir(tmp.hg_repository_name)
					system_halt_on_error(['hg', 'pull'])
					os.chdir(work_dir)
				try:
					os.chdir(tmp.hg_repository_name)
					tmp.hg_repository_extra_function()
					os.chdir(work_dir)
				except: pass
			except:
				print '- No hg repository'
			print ''
			
			# Build
			print 'How build:', tmp.how_build
			# BUILD_WITH_CMAKE
			if (tmp.how_build == 'BUILD_WITH_CMAKE'):
				build_with_cmake(build_directory, install_directory)
			# BUILD_WITH_AUTOTOOLS
			elif (tmp.how_build == 'BUILD_WITH_AUTOTOOLS'):
				build_with_autotools(build_directory, install_directory, tmp.how_build_autotools_configure_args_list(install_directory))
			# CUSTOM_BUILD
			elif (tmp.how_build == 'CUSTOM_BUILD'):
				tmp.how_build_function(install_directory)
			else:
				print '- error: "' + tmp.how_build + '" is unknow'
			
			i = i + 1
	
	# search
	elif (command == 'search'):
		
		
		if (len(sys.argv) > 2):
		
			package_name_searched = sys.argv[2]
			
			from about_packages import package_description
			
			print ''
			print 'Search packages "' + package_name_searched + '":'
			print ''
			
			for file in os.listdir(os.path.join(filesystem.app_data('install_projects'), 'packages')):
				
				if (len(file) > 3 and file[len(file) - 3 : ] == '.py'):
					
					if (file != 'about_packages.py' and file != '__init__.py' and file != 'tmp.py'):
					
						package_name = file[ : len(file) - 3]
						
						if (package_name_searched in package_name):
							print package_name
							print package_description(package_name)
							print ''
	
	# list
	elif (command == 'list'):
		
		from about_packages import package_description
		
		print ''
		print 'List of all packages:'
		print ''
		
		for file in os.listdir(os.path.join(filesystem.app_data('install_projects'), 'packages')):
			
			if (len(file) > 3 and file[len(file) - 3 : ] == '.py'):
				
				if (file != 'about_packages.py' and file != '__init__.py' and file != 'tmp.py'):
				
					package_name = file[ : len(file) - 3]
					
					print package_name
					print package_description(package_name)
					print ''
	
	# clean
	elif (command == 'clean'):
		
		print ''
		
		removed = False
		
		for file in os.listdir(filesystem.app_data('install_projects')):
			
			path_file = os.path.join(filesystem.app_data('install_projects'), file)
			
			if (os.path.isdir(path_file)):
				
				if (os.path.exists(os.path.join(path_file, 'build'))):
					
					print 'Remove "' + os.path.join(path_file, 'build') + '"'
					filesystem.rm_directory(os.path.join(path_file, 'build'))
					removed = True
		
		if (removed): print ''
	
	# clear
	elif (command == 'clear'):
		
		removed = False
		
		for file in os.listdir(filesystem.app_data('install_projects')):
			
			if (file != 'packages' and file != 'make_life_simpler'):
			
				path_file = os.path.join(filesystem.app_data('install_projects'), file)
			
				if (os.path.isdir(path_file)):
				
					print 'Remove "' + path_file + '"'
					filesystem.rm_directory(path_file)
					removed = True
			
				elif (len(file) > 4 and (file[len(file) - 4:len(file)] == '.zip' or file[len(file) - 4:len(file)] == '.pyc')):
					
					print 'Remove "' + path_file + '"'
					os.remove(path_file)
					removed = True
		
		if (removed): print ''
	
	# reset
	elif (command == 'reset'):
		
		removed = False
		
		for file in os.listdir(filesystem.app_data('install_projects')):
			
			path_file = os.path.join(filesystem.app_data('install_projects'), file)
			
			print 'Remove "' + path_file + '"'
			if (os.path.isdir(path_file)): filesystem.rm_directory(path_file)
			else: os.remove(path_file)
			removed = True
		
		if (removed): print ''
		
	# unknow command
	else:
		
		print 'Error: unknow command "' + command + '"'
		
		os.chdir(current_dir)
		display_help()
	
	
	# Restore original current directory
	os.chdir(current_dir)
	
	# Exit
	sys.exit(0)
